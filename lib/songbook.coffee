SongbookView = require './songbook-view'
{CompositeDisposable} = require 'atom'

module.exports = Songbook =
  songbookView: null
  modalPanel: null
  subscriptions: null

  activate: (state) ->
    @songbookView = new SongbookView(state.songbookViewState)
    @modalPanel = atom.workspace.addModalPanel(item: @songbookView.getElement(), visible: false)

    # Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    @subscriptions = new CompositeDisposable

    # Register command that toggles this view
    @subscriptions.add atom.commands.add 'atom-workspace', 'songbook:toggle': => @toggle()

  deactivate: ->
    @modalPanel.destroy()
    @subscriptions.dispose()
    @songbookView.destroy()

  serialize: ->
    songbookViewState: @songbookView.serialize()

  toggle: ->
    console.log 'Songbook was toggled!'

    if @modalPanel.isVisible()
      @modalPanel.hide()
    else
      @modalPanel.show()
